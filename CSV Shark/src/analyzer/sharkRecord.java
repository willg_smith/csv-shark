package analyzer;
public class sharkRecord{
    int number;
    double time;
    //String name = record[1];
    String source;
    String destination;
    String protocol;
    int length;
    String info;
    public sharkRecord(int newID, double newTime, String newSource,
    String newDestination, String newProtocol, int newLength, String newInfo){
        // initialise instance variables
        number = newID;
        time = newTime;
        source = newSource;
        destination = newDestination;
        protocol = newProtocol;
        length = newLength;
        info = newInfo;
    }
    
    public int getLength() {
        return length;
    }
}
