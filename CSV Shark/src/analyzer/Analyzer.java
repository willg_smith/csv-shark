package analyzer;
import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author William Gage Smith and Derek Halder
 *
 */
public class Analyzer {
    
    private List<Record> records;
    //public List<sharkRecord> wireRec;
    private List<Protocol> protocols = new ArrayList<Protocol>();
    ArrayList<Address> clients = new ArrayList<Address>();
    boolean protocolsAnalyzed = false;
    boolean addressesAnalyzed = false;
    
    /**
     * Takes an arraylist of record objects. 
     * @param list
     *///sharkRecord
    public Analyzer(ArrayList<Record> list) {
        this.records = list;
    }
    
    /**
     * just an adapter as the view controller was not working 
     * for me
     */
    public void Analyzer2(ArrayList<sharkRecord> list) {
    	records = new ArrayList<Record>();
        for(int i = 0; i < list.size(); i++){
            sharkRecord sr = list.get(i);
            Record newRec = new Record(sr.number, sr.time, sr.source,
            sr.destination, sr.protocol, sr.length, sr.info);
            records.add(newRec);
        }
    }
    
    @SuppressWarnings("unused")
    private Analyzer() {}
    
    public int recordCount(){
        return records.size();
    }
    
    /**
     * Returns smallest packet size in data set
     * @return
     */
    public int smallestPacketSize() {
        int result = Integer.MAX_VALUE;
        for(Record r : records) {
            if (r.getLength() < result) {
                result = r.getLength();
            }
        }
        return result;
    }
    
    /**
     * Returns smallest packet in data set
     * @return
     */
    public int smallestPacket(){
        int value = Integer.MAX_VALUE;
        Record result = null;
        for(Record r : records) {
            if (r.getLength() < value) {
                value = r.getLength();
                result = r;
            }
        }
        return value;
    }
    
    public int averagePacketSize(){
        int count = 0;
        for(int i = 0; i < records.size(); i++){
            count = count + records.get(i).getLength();
        }
        int result = count/records.size();
        return result; 
    }
    
    public int largestPacketSize() {
        int result = Integer.MIN_VALUE;
        for(Record r : records) {
            if (r.getLength() > result) {
                result = r.getLength();
            }
        }
        return result;
    }
    
    public Record largestPacket(){
        int value = Integer.MAX_VALUE;
        Record result = null;
        for(Record r : records) {
            if (r.getLength() > value) {
                value = r.getLength();
                result = r;
            }
        }
        return result;
    }
    
    public double getPercentageOf(String type){
    	
    	if (!protocolsAnalyzed) {
    		analyzeProtocols();
    	}
    	
        int total = 0; int packAmount;
        ArrayList<String> uniqueProtocol = new ArrayList<String>();
        ArrayList<Integer> senderCount = new ArrayList<Integer>();
        double percent = 0.00;
        
        for(int i = 0; i < protocols.size(); i++){
            total = total + protocols.get(i).getCount();
            uniqueProtocol.add(protocols.get(i).getName());
            senderCount.add(protocols.get(i).getCount());
        }
        
        if( uniqueProtocol.contains(type) ){
            int location = uniqueProtocol.indexOf(type);
            packAmount = senderCount.get(location);
            percent = (double) (packAmount * 100)/total;
        }
        //contains
        return percent;
    }
    
    public void generateAdressData(){
        ArrayList<String> clientsNames = new ArrayList<String>();
        int senderCount = 0;
        
        for(int i = 0; i < records.size(); i++){
            if(  !( clientsNames.contains(records.get(i).getSource() ) )   ){
                
                clientsNames.add(   records.get(i).getSource()  );
                Address newClient = new Address(records.get(i).getSource() );
                clients.add(    newClient   );
                
            }
            
            if(  !( clientsNames.contains(records.get(i).getDestination() ) )   ){
                
                clientsNames.add(   records.get(i).getDestination()  );
                Address newClient = new Address(records.get(i).getDestination() );
                clients.add(    newClient   );
                
            }
            
            if(  ( clientsNames.contains(records.get(i).getSource() ) )   ){
                
                String name = records.get(i).getSource();
                int location = clientsNames.indexOf(name);
                clients.get(location).sendCount++;
                
            }
            
            if(  ( clientsNames.contains(records.get(i).getDestination() ) )   ){
                
                String name = records.get(i).getDestination();
                int location = clientsNames.indexOf(name);
                clients.get(location).recieveCount++;
                
            }
        }
        
        addressesAnalyzed = true;
    }
    
    /**
     * how many address entities exist that recieve
     */
    public int getSenderCount(){
        if(!addressesAnalyzed){
            generateAdressData();
        }
        int sent = 0;
        for(int i = 0; i < clients.size(); i++){
            if(clients.get(i).sendCount > 0){
                sent++;
            }
        }
        return sent;
    }
    
    /**
     * how many address entities exist that recieve data
     */
    public int getRecieverCount(){
        if(!addressesAnalyzed){
            generateAdressData();
        }
        int recieved = 0;
        for(int i = 0; i < clients.size(); i++){
            if(clients.get(i).recieveCount > 0){
                recieved++;
            }
        }
        return recieved;
    }
    
    /**
     * how many unique address entities exist in the data set
     */
    public int getUniqueAddressCount(){
    	if(!addressesAnalyzed){
            generateAdressData();
        }
        return clients.size();
    }
    
    public String predominantProtocol() {
        String result = "";
        int count = 0;
        analyzeProtocols();
        for(Protocol p : protocols) {
            if (p.getCount() > count) {
                count = p.getCount();
                result = p.getName();
            }
        }
        return result;
    }
    
    /**
     * This will get the count of each protocol and store it in the protocols array list. 
     * If the protol is not used, it will not be in the arraylist.
     * This will make it easier to analyze the protocols. 
     * This will also only happen once. 
     */
    public void analyzeProtocols(){
        ArrayList<String> uniqueProtocol = new ArrayList<String>();
        ArrayList<Integer> senderCount = new ArrayList<Integer>();
        
        for(int i = 0; i < records.size(); i++){
            if(  !( uniqueProtocol.contains(records.get(i).getProtocol() ) ) ){
                uniqueProtocol.add( records.get(i).getProtocol() );
                senderCount.add( 1 );
            } else {
                int location = uniqueProtocol.indexOf( records.get(i).getProtocol() );
                int update = senderCount.get( location );
                update++;
                senderCount.set( location, update );
            }
        }
        
        for(int i = 0; i < uniqueProtocol.size(); i++){
            Protocol newPro 
                = new Protocol(uniqueProtocol.get(i), senderCount.get(i));
            protocols.add(newPro);
        }
    }
    
    
}
