package analyzer;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

public class Receiver {
	
	private SimpleStringProperty receiver;
	private SimpleIntegerProperty messages;
	
	public Receiver(String receiver, int messages) {
		this.receiver = new SimpleStringProperty(receiver);
		this.messages = new SimpleIntegerProperty(messages);
	}

	public String getReceiver() {
		return receiver.get();
	}

	public int getMessages() {
		return messages.get();
	}

}
