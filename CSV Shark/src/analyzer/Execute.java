package analyzer;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class Execute extends Application {

	public static void main(String[] args) {
		launch(args);

	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		Parent root = FXMLLoader.load(getClass().getResource("View.fxml"));
		primaryStage.setTitle("Packet Analysis");
	    primaryStage.setScene(new Scene(root));
	    //primaryStage.initStyle(StageStyle.TRANSPARENT);
	    //primaryStage.getIcons().add(new Image("application icon here"));
	    primaryStage.show();
		
	}

}
