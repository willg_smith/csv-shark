package analyzer;
public class Protocol {
    private String name;
    private int count;
    public double percent;
	
    public Protocol(String name, int count) {
        this.count = count;
        this.name = name;
    }
	
    public String getName() {
        return name;
    }
    
    public int getCount() {
        return count;
    }

}
