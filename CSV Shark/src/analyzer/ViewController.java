package analyzer;
import java.io.File;
import java.io.IOException;
import java.io.Reader;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.ResourceBundle;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.chart.PieChart;
import javafx.scene.control.Button;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.stage.Window;

/**
 * 
 * @author William Gage Smith
 *
 */
public class ViewController implements Initializable {
	
	////////////////////////// VISUALS ////////////////////////
	
	/////////////// PIE CHART

    @FXML
    private PieChart protocolChart;
    
    /////////////// SENDERS TABLE

    @FXML
    private TableView<Sender> sendersTable;

    @FXML
    private TableColumn<Sender, String> senderColumn;

    @FXML
    private TableColumn<Sender, Integer> senderMessagesColumn;

    /////////////// RECEIVERS TABLE
    @FXML
    private TableView<Receiver> receiversTable;

    @FXML
    private TableColumn<Receiver, String> receiverColumn;

    @FXML
    private TableColumn<Receiver, Integer> receiverMessagesColumn;
	
    ////////////////////////// END VISUALS ////////////////////////
    
    ////////////////////////// VIEW ATTRIBUTES ////////////////////////
    
    @FXML
    private Label selectLabel;
    
    @FXML
    private Label currentFile;

    @FXML
    private Button chooseFile;
    
    @FXML
    private Label filenameLabel;
    
    @FXML
    private Label mostUsedProtocolLabel;

    @FXML
    private Label mostUsedProtocolResult;

    @FXML
    private Label largestPacketLabel;

    @FXML
    private Label largestPacketResult;
    
    @FXML
    private Label smallestPacketResult;
    
    @FXML
    private Label averagePacketResult;
    
    @FXML
    private Label uniqueAddressesResult;
    
    @FXML
    private Label sendersResult;
    
    @FXML
    private Label receiversResult;
    
    ////////////////////////// END VIEW ATTRIBUTES ////////////////////////
    
    ////////////////////////// OBJECT AIDS ////////////////////////
    
    private ObservableList<Sender> senders = FXCollections.observableArrayList();
    private ObservableList<Receiver> receivers = FXCollections.observableArrayList();
    private ObservableList<PieChart.Data> pieChartData = FXCollections.observableArrayList();
    ArrayList<Record> records;
    ArrayList<PieChart.Data> pieChartItems;
    Analyzer analyzer;
    recordBuilder recordBuilder;
    
    ////////////////////////// END OBJECT AIDS ////////////////////////

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		initializeTable();
		currentFile.setVisible(false);
		
	}
	
	private void initializeTable() {
		senderColumn.setCellValueFactory(new PropertyValueFactory<>("sender"));
		senderMessagesColumn.setCellValueFactory(new PropertyValueFactory<>("messages"));
		receiverColumn.setCellValueFactory(new PropertyValueFactory<>("receiver"));
		receiverMessagesColumn.setCellValueFactory(new PropertyValueFactory<>("messages"));
		
	}

	public void chooseFile() {
		
		records = new ArrayList<Record>();
		String filename = "";
		
		try {
			FileChooser filechooser = new FileChooser();
			File input = filechooser.showOpenDialog(chooseFile.getScene().getWindow());
			filename = input.getName();
			filenameLabel.setText(filename + " selected");
			recordBuilder = new recordBuilder(input);
			Reader reader = Files.newBufferedReader(Paths.get(input.getAbsolutePath()));
			
			CSVParser parser = new CSVParser(reader, CSVFormat.DEFAULT
					.withFirstRecordAsHeader()
                    .withIgnoreHeaderCase()
                    .withTrim());
			
			for (CSVRecord record : parser) {
				int number = Integer.parseInt(record.get("No."));
				double time = Double.parseDouble(record.get("Time"));
				String source = record.get("Source");
				String destination = record.get("Destination");
				String protocol = record.get("Protocol");
				int length = Integer.parseInt(record.get("Length"));
				String info = record.get("Info");
				
				Record temp = new Record(number, time, source, destination, protocol, length, info);
				records.add(temp);
				
			}
			
			analyzer = new Analyzer(records);
			
		} catch (IOException e) {
			System.out.println("Error reading file.");
			e.printStackTrace();
		}
		
		uploadSuccessful();
		setTableData();
		setPieData();
		currentFile.setText("Current file: " + filename);
		currentFile.setVisible(true);
		
	}
	
	private void setPieData() {
		ArrayList<Protocol> list = recordBuilder.protocolCountAnalysis();
		pieChartItems = new ArrayList<PieChart.Data>();
		double sum = 0.00;
		for (int i = 0; i < list.size(); i++) {
			pieChartItems.add(new PieChart.Data(list.get(i).getName().toString(), list.get(i).percent));
			sum += list.get(i).percent;
		}
		
		double remaining = 100.00 - sum;
		pieChartItems.add(new PieChart.Data("Other", remaining));
		
		pieChartData.setAll(pieChartItems);		
		protocolChart.getData().clear();		
		protocolChart.getData().addAll(pieChartData);
		
	}

	private void setTableData() {
		// Senders Table
		senders.setAll(recordBuilder.sentNumbersTable());
		sendersTable.setItems(senders);
		
		//Receivers Table
		receivers.setAll(recordBuilder.desinationNumbersTable());
		receiversTable.setItems(receivers);
		
	}

	private void uploadSuccessful() {
		
		setMostUsedProtocol();
		setLargestPacket();
		setSmallestPacket();
		setAveragePacket();
		setUniqueAddresses();
		setSenders();
		setReceivers();
	}

	private void setReceivers() {
		receiversResult.setText(Integer.toString(analyzer.getRecieverCount()));
	}

	private void setSenders() {
		sendersResult.setText(Integer.toString(analyzer.getSenderCount()));
	}

	private void setUniqueAddresses() {
		uniqueAddressesResult.setText(Integer.toString(analyzer.getUniqueAddressCount()));
	}

	private void setAveragePacket() {
		averagePacketResult.setText(Integer.toString(analyzer.averagePacketSize()));
	}

	private void setSmallestPacket() {
		smallestPacketResult.setText(Integer.toString(analyzer.smallestPacket()));
	}

	private void setLargestPacket() {
		largestPacketResult.setText(analyzer.largestPacketSize() + " bytes");
	}

	private void setMostUsedProtocol() {
		mostUsedProtocolResult.setText(analyzer.predominantProtocol());
		
	}


}
