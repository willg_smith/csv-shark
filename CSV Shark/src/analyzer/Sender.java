package analyzer;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

public class Sender {
	
	private SimpleStringProperty sender;
	private SimpleIntegerProperty messages;
	
	public Sender(String sender, int messages) {
		this.sender = new SimpleStringProperty(sender);
		this.messages = new SimpleIntegerProperty(messages);
	}

	public String getSender() {
		return sender.get();
	}

	public int getMessages() {
		return messages.get();
	}

}
