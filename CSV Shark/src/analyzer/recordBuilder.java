package analyzer;
import java.util.*;
import java.lang.*;
import java.text.DecimalFormat;
import java.io.*;

/**
 * 
 * @author William Gage Smith and Derek Halder
 *
 */
public class recordBuilder{
    //fileReader getInfo;
    ArrayList<sharkRecord> wireRec = new ArrayList<sharkRecord>();
    ArrayList<Protocol> largest = new ArrayList<Protocol>();
    ArrayList<String> uniqueProtocol;
    ArrayList<Integer> senderCount;
    private static DecimalFormat format = new DecimalFormat("#.##");
    
    public recordBuilder(){//fileReader newInfo
        buildSharks();
    } 
    
    public recordBuilder(File input){//fileReader newInfo
        buildSharks(input);
    } 
    
    //Integer.parseInt(record[2]); Double.parseDouble(record[4]);
    public void buildSharks(){
        try{
            FileReader out = new FileReader("wiresharkdata.csv");
            BufferedReader out2 = new BufferedReader(out);
            CSVReader reader = new CSVReader(out2);
            List<String[]> records = reader.readAll();
            for (int i = 0; i < records.size(); i++) {
                String[] record = records.get(i);
                if(i != 0){
                    int number = Integer.parseInt(record[0]);
                    double time = Double.parseDouble(record[1]);
                    //String name = record[1];
                    String source = record[2];
                    String destination = record[3];
                    String protocol = record[4];
                    int length = Integer.parseInt(record[5]);
                    String info = record[6];
                    sharkRecord newRec = new sharkRecord(number, time,
                    source, destination, protocol, length, info);
                    wireRec.add(newRec);
                }
            }
        } catch(Exception e) {
            e.printStackTrace();
        }
    }
    
    public void buildSharks(File file){
        try{
            FileReader out = new FileReader(file);
            BufferedReader out2 = new BufferedReader(out);
            CSVReader reader = new CSVReader(out2);
            List<String[]> records = reader.readAll();
            for (int i = 0; i < records.size(); i++) {
                String[] record = records.get(i);
                if(i != 0){
                    int number = Integer.parseInt(record[0]);
                    double time = Double.parseDouble(record[1]);
                    //String name = record[1];
                    String source = record[2];
                    String destination = record[3];
                    String protocol = record[4];
                    int length = Integer.parseInt(record[5]);
                    String info = record[6];
                    sharkRecord newRec = new sharkRecord(number, time,
                    source, destination, protocol, length, info);
                    wireRec.add(newRec);
                }
            }
        } catch(Exception e) {
            e.printStackTrace();
        }
    }
    
    public void smallest(){
        if(wireRec.size()==0){
            return;
        }
        sharkRecord longestRec = wireRec.get(0);
        sharkRecord longestRec2 = wireRec.get(0);
        int counter = 0;
        for(int i = 0; i < wireRec.size(); i++){
            
            if(longestRec.length == wireRec.get(i).length){
                counter++;
            }
            if(longestRec.length > wireRec.get(i).length){
                longestRec = wireRec.get(i);
                counter = 1;
            }
            if(longestRec2.length >= wireRec.get(i).length){
                longestRec2 = wireRec.get(i);
                counter++;
            }
            
        }
        
        if( longestRec.number != longestRec2.number ){
            System.out.println("Message "+ longestRec.number + 
            " was the smallest and first of its length with: "
            + longestRec.length);
            System.out.println("Message "+ longestRec2.number + 
            " was the smallest and last of its length with: "
            + longestRec2.length);
            System.out.println("There are " + counter
            + " messages of the smallest length");
        } else {
            System.out.println("Message "+ longestRec.number + 
            " was the smallest single message of length with: "
            + longestRec.length);
        }
        
    }
    
    public void longest(){
        if(wireRec.size()==0){
            return;
        }
        sharkRecord longestRec = wireRec.get(0);
        sharkRecord longestRec2 = wireRec.get(0);
        int counter = 0;
        for(int i = 0; i < wireRec.size(); i++){
            if(longestRec.length == wireRec.get(i).length){
                counter++;
            }
            if(longestRec.length < wireRec.get(i).length){
                longestRec = wireRec.get(i);
                counter = 1;
            }
            if(longestRec2.length <= wireRec.get(i).length){
                longestRec2 = wireRec.get(i);
                counter++;
            }
        }
        
        if( longestRec.number != longestRec2.number ){
            System.out.println("Message "+ longestRec.number + 
            " was the longest and first of its length with: "
            + longestRec.length);
            System.out.println("Message "+ longestRec2.number + 
            " was the longest and last of its length with: "
            + longestRec2.length);
            System.out.println("There are " + counter
            + " messages of the longest length");
        } else {
            System.out.println("Message "+ longestRec.number + 
            " was the longest single message of length with: "
            + longestRec.length);
        }
        
    }
    
    public int recSize(){
        return wireRec.size();
    }
    
    public void sentNumbers(){
        ArrayList<String> uniqueSenders = new ArrayList<String>();
        ArrayList<Integer> senderCount = new ArrayList<Integer>();
        
        for(int i = 0; i < wireRec.size(); i++){
            if(  !( uniqueSenders.contains(wireRec.get(i).source) ) ){
                uniqueSenders.add( wireRec.get(i).source );
                senderCount.add( 1 );
            } else {
                int location = uniqueSenders.indexOf( wireRec.get(i).source );
                int update = senderCount.get( location );
                update++;
                senderCount.set( location, update );
            }
        }
        
        for(int i = 0; i < uniqueSenders.size(); i++){
            System.out.println(uniqueSenders.get(i) +" has sent: " + 
            senderCount.get(i) + " messages.");
        }
    }
    
    public ArrayList<Sender> sentNumbersTable(){
        ArrayList<String> uniqueSenders = new ArrayList<String>();
        ArrayList<Integer> senderCount = new ArrayList<Integer>();
        
        for(int i = 0; i < wireRec.size(); i++){
            if(  !( uniqueSenders.contains(wireRec.get(i).source) ) ){
                uniqueSenders.add( wireRec.get(i).source );
                senderCount.add( 1 );
            } else {
                int location = uniqueSenders.indexOf( wireRec.get(i).source );
                int update = senderCount.get( location );
                update++;
                senderCount.set( location, update );
            }
        }
        
        ArrayList<Sender> result = new ArrayList<Sender>();
        
        for(int i = 0; i < uniqueSenders.size(); i++){
        	Sender temp = new Sender(uniqueSenders.get(i), senderCount.get(i));
        	result.add(temp);
        }
        return result;
    }
    
    public void desinationNumbers(){
        ArrayList<String> uniqueRecievers = new ArrayList<String>();
        ArrayList<Integer> senderCount = new ArrayList<Integer>();
        
        for(int i = 0; i < wireRec.size(); i++){
            if(  !( uniqueRecievers.contains(wireRec.get(i).destination) ) ){
                uniqueRecievers.add( wireRec.get(i).destination );
                senderCount.add( 1 );
            } else {
                int location = uniqueRecievers.indexOf( wireRec.get(i).destination );
                int update = senderCount.get( location );
                update++;
                senderCount.set( location, update );
            }
        }
        
        for(int i = 0; i < uniqueRecievers.size(); i++){
            System.out.println(uniqueRecievers.get(i) +" was sent: " + 
            senderCount.get(i) + " messages.");
        }
    }
    
    public ArrayList<Receiver> desinationNumbersTable(){
        ArrayList<String> uniqueRecievers = new ArrayList<String>();
        ArrayList<Integer> senderCount = new ArrayList<Integer>();
        
        for(int i = 0; i < wireRec.size(); i++){
            if(  !( uniqueRecievers.contains(wireRec.get(i).destination) ) ){
                uniqueRecievers.add( wireRec.get(i).destination );
                senderCount.add( 1 );
            } else {
                int location = uniqueRecievers.indexOf( wireRec.get(i).destination );
                int update = senderCount.get( location );
                update++;
                senderCount.set( location, update );
            }
        }
        
        ArrayList<Receiver> result = new ArrayList<Receiver>();
        
        for(int i = 0; i < uniqueRecievers.size(); i++){
        	Receiver temp = new Receiver(uniqueRecievers.get(i), senderCount.get(i));
        	result.add(temp);
        }
        
        return result;
    }
    
    public void clientNumbers(){
        ArrayList<String> uniqueRecievers = new ArrayList<String>();
        ArrayList<String> uniqueSenders = new ArrayList<String>();
        int senderCount = 0;
        
        for(int i = 0; i < wireRec.size(); i++){
            
            if(  !( uniqueSenders.contains(wireRec.get(i).source) ) ){
                uniqueSenders.add( wireRec.get(i).source );
                senderCount++;
            }
            
            if(  !( uniqueRecievers.contains(wireRec.get(i).destination) ) ){
                uniqueRecievers.add( wireRec.get(i).destination );
                if( !( uniqueSenders.contains(wireRec.get(i).destination) ) ){
                    senderCount++;
                }
                
            }
            
        }
        
        System.out.println("There were " + uniqueSenders.size() 
        + " senders");
        System.out.println("There were " + uniqueRecievers.size() 
        + " clients");
        System.out.println("There were " + senderCount 
        + " uniqe enties in these transaction");
    }
    
    public void protocolCount(){
        ArrayList<String> uniqueProtocol = new ArrayList<String>();
        ArrayList<Integer> senderCount = new ArrayList<Integer>();
        
        for(int i = 0; i < wireRec.size(); i++){
            if(  !( uniqueProtocol.contains(wireRec.get(i).protocol) ) ){
                uniqueProtocol.add( wireRec.get(i).protocol );
                senderCount.add( 1 );
            } else {
                int location = uniqueProtocol.indexOf( wireRec.get(i).protocol );
                int update = senderCount.get( location );
                update++;
                senderCount.set( location, update );
            }
        }
        
        for(int i = 0; i < uniqueProtocol.size(); i++){
            System.out.println(uniqueProtocol.get(i) + " protocol was used for: " + 
            senderCount.get(i) + " messages.");
        }
    }
	
    /**
     * Returns arraylist of 10 most used protocols with count
     * @return
     */
    public ArrayList<Protocol> protocolCountAnalysis(){
        ArrayList<String> uniqueProtocol = new ArrayList<String>();
        ArrayList<Integer> senderCount = new ArrayList<Integer>();
        ArrayList<Protocol> result = new ArrayList<Protocol>();
        
        for(int i = 0; i < wireRec.size(); i++){
            if(  !( uniqueProtocol.contains(wireRec.get(i).protocol) ) ){
                uniqueProtocol.add( wireRec.get(i).protocol );
                senderCount.add( 1 );
            } else {
                int location = uniqueProtocol.indexOf( wireRec.get(i).protocol );
                int update = senderCount.get( location );
                update++;
                senderCount.set( location, update );
            }
        }
        
        int totalData = wireRec.size();
        
        int count = -1;
        int largest = 0;
        int largestIndex = 0;
        for (int i = 0; i < uniqueProtocol.size(); i++) {
			count++;
			largest = 0;
			if (count > 5) break;
			for (int j = 0; j < uniqueProtocol.size(); j++) {
				if (senderCount.get(j) > largest) {
					largest = senderCount.get(j);
					largestIndex = j;
				}
			}
			result.add(new Protocol(uniqueProtocol.get(largestIndex), largest));
			uniqueProtocol.remove(largestIndex);
			senderCount.remove(largestIndex);
		}
        
        double amount = 0;
        for (int i = 0; i < result.size(); i++) {
        	amount = result.get(i).getCount();
        	result.get(i).percent = (amount*100)/totalData;
        	
		}        
        return result;
    }

}
