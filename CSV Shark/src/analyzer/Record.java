package analyzer;

public class Record {
	
	private int number;
	private double time;
	private String source;
	private String destination;
	private String protocol;
	private int length;
	private String info;
	
	public Record(int number, double time, String source, String destination, String protocol, int length, String info) {
		this.number = number;
		this.time = time;
		this.source = source;
		this.destination = destination;
		this.protocol = protocol;
		this.length = length;
		this.info = info;
	}
	
	@SuppressWarnings("unused")
	private Record() {}

	public int getNumber() {
		return number;
	}

	public double getTime() {
		return time;
	}

	public String getSource() {
		return source;
	}

	public String getDestination() {
		return destination;
	}

	public String getProtocol() {
		return protocol;
	}

	public int getLength() {
		return length;
	}

	public String getInfo() {
		return info;
	}

}
